Both my Individual and Group projects are in this directory, because I did the group project alone.
Aaron Handleman (444509)
Chatroom Project
======================

 > Calendar View
    - The Chatroom allows the user to enter different rooms
    - Users can also create rooms, and set passwords on them at the time of creation.

 > Best Practices
    - Code is easy to navigate
    - Highly object oriented for easy modualarity
    - Content is escaped on input and output
    - All in one session/never reloads page.



 > Usability
    - Very Usable, with instructions given in a friendly, light hearted manner
    - Simple, elegent design



 > Creative Portion
    - Admins can transfer ownership of the room to another user, relinquishing their control.
    - Users can change names without reloading the page
    - Timestamps on all messages
    - Users can type /airhorn, /skelly, /lol, /wow, /friends for fun soundclips (lower your volume).  This works in both private and public messages.  All users who would recieve the message instead hear the soundclip.


 > Hosted at www.pepexchange.com:3456 for Chatroom
 > Hosted at www.pepexchange.com:4567 for Fileserver